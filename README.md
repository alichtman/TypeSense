## TypeSense
---

TypeSense is a Chrome extension that displays the sentiment impact of each message in a Facebook Messenger conversation in real-time. Winner of the best Healthcare-related OSS hack at HackIllinois 2018.

##### Why?
---

Conversations can be complicated. With an increasing reliance on digital communication, the body language and intonation cues we use to interpret meaning in someone’s words quickly disappear. People without psychological disorders misunderstand each other all the time. Sarcasm makes it hard to know for sure what someone means, and idioms are confusing! _Imagine how hard it is for people with social difficulties._

[TypeSense serves to simplify social interaction? help people respond appropriately to incoming messages? idk someone help ]

Designed to help users with pervasive developmental disorders, like Aspergers or Autism.


##### Project Features
---

+ Visualization of the impact of each message on conversation sentiment
+ Seamless integration with Messenger in Chrome
+ Intuitive, simple interface.
+ Preview impact of messages on conversation sentiment before they're sent


##### Usage
---

1. Download a `.zip` of the repo and unzip it
2. Open Chrome and enter `chrome://extensions` into the address bar
4. Enable `Developer Mode`
5. Click `Load Unpacked Extension` and select extracted folder
6. Make sure `TypeSense` is enabled
7. Navigate to www.messenger.com and click the TypeSense extension button


##### How to Contribute
---

1. Clone the repo
2. Create a new branch: `$ git checkout https://github.com/shobrook/TypeSense -b [name_for_new_branch]`.
3. Open Github Issue detailing the feature/bug/improvement
4. Write code, test, and open a Pull Request with comprehensive description of changes


##### Made By
---

+ Aaron Lichtman
+ Adam Gernes
+ Jonathan Shobrook
+ Rishi Masand

#### License
---

MIT
